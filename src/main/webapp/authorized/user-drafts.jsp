<%--
  Created by IntelliJ IDEA.
  User: Volodymyr
  Date: 02.07.2018
  Time: 15:22
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>My Drafts</title>
    <link rel="stylesheet" href="./assets/style.css" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <%--For fa-fa icons--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="${pageContext.request.contextPath}/blog">BLOG</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/signout"><span class="glyphicon glyphicon-log-out"></span> Sign Out</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <li><a href="${pageContext.request.contextPath}/posts"><i class="far fa-newspaper"></i> My Posts</a></li>
                <li class="nav-item active"><a href="${pageContext.request.contextPath}/drafts"><i class="fas fa-pen-square"></i> My Drafts</a></li>
                <li><a href="${pageContext.request.contextPath}/addpost"><i class="fas fa-plus"></i> Add</a></li>
            </ul>
        </div>
    </nav>
</header>

<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <c:forEach items="${requestScope.usersDrafts}" var="post">
                    <div class="blog-post">
                        <h3 class="blog-post-title">${post.title}</h3>
                        <ul class="blog-post-info">
                            <li><i class="fas fa-user"></i>${post.author.userName}</li>
                            <li><i class="fa fa-calendar" aria-hidden="true"></i>${post.dateOfCreation}</li>
                            <li><i class="fa fa-tag" aria-hidden="true"></i> <c:forEach items="${post.tags}" var="tag">${tag.tag} </c:forEach></li>
                        </ul>
                        <div class="blog-post-date">
                            <h1>${post.dateOfPublication.getDayOfMonth()}</h1>
                            <h5>${post.dateOfPublication.getMonth()} ${post.dateOfPublication.getYear()}</h5>
                        </div>
                        <img class="img-responsive blog-img" src=${post.imageURL}>
                        <p>${post.description}<p>
                        <a class="button green btn btn-success col-md-2" href="${pageContext.request.contextPath}/post?postId=${post.id}">Read More</a>
                        <a class="button green-right btn btn-success col-md-2" data-toggle="modal" data-target="#modalDeleteId${post.id}">Delete</a>
                        <a class="button green-right btn btn-success col-md-2" href="${pageContext.request.contextPath}/editpost?postId=${post.id}">Edit</a>
                        <a class="button green-right btn btn-success col-md-2" data-toggle="modal" data-target="#modalPublishDraftId${post.id}">Publish</a>
                    </div>
                    <div id="modalDeleteId${post.id}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Delete Confirm</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to delete this draft?</p>
                                </div>
                                <div class="modal-footer">
                                    <a class="button btn btn-success col-md-2" href="${pageContext.request.contextPath}/delete?postId=${post.id}">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="modalPublishDraftId${post.id}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Publish Confirm</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to publish this draft?</p>
                                </div>
                                <div class="modal-footer">
                                    <a class="button btn btn-success col-md-2" href="${pageContext.request.contextPath}/publishdraft?draftId=${post.id}">Publish</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</section>

<!--jQuery plugin-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
