<%--
  Created by IntelliJ IDEA.
  User: Volodymyr
  Date: 02.07.2018
  Time: 15:22
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Tags</title>
    <link rel="stylesheet" href="./assets/style.css" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <%--For fa-fa icons--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="${pageContext.request.contextPath}/users">Admin</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/signout"><span class="glyphicon glyphicon-log-out"></span> Sign Out</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <li><a href="${pageContext.request.contextPath}/users"><i class="fa fa-users" aria-hidden="true"></i> Users</a></li>
                <li class="nav-item active"><a href="${pageContext.request.contextPath}/tags"><i class="fas fa-tags"></i> Tags</a></li>
            </ul>
        </div>
    </nav>
</header>

<section id="blog">
    <div class="container">
        <%--<div class="panel panel-default">--%>
            <%--<div class="panel-heading">--%>
                <button data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" type="button" class="btn btn-success btn-add-tag"><i class="fa fa-plus" aria-hidden="true"></i> Tag</button>
            <%--</div>--%>
        <%--</div>--%>
        <div id="accordion">
            <div class="card">
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                        <hr>
                        <h4>Add Tag:</h4>
                        <form method="post" action="${pageContext.request.contextPath}/addtag" id="addTagForm">
                            <input required class="form-control" type="text" name="tag" id="tag" placeholder="Tag name"><hr>
                            <input type="submit" value="Add" class="btn btn-success btn-add-tag">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Tag</th>
                <th class="text-right">Action</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.tags}" var="tag">
                <tr>
                    <td>${tag.id}</td>
                    <td>${tag.tag}</td>
                    <td class="text-right">
                        <a class="button btn btn-warning" data-toggle="modal" data-target="#modalUpdateTag${tag.id}">Update</a>
                    </td>
                </tr>
                <div id="modalUpdateTag${tag.id}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update Tag</h4>
                            </div>
                            <form method="post" action="${pageContext.request.contextPath}/edittag?tagId=${tag.id}" id="editTagForm">
                                <div class="modal-body">
                                    <input required type="text" class="form-control" id="updateTag" name="updateTag" placeholder="New tag name" value="${tag.tag}">
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" value="Update" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </c:forEach>
            </tbody>
        </table>
    </div>
</section>

<!--jQuery plugin-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
