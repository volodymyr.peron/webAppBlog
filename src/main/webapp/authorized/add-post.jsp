<%--
  Created by IntelliJ IDEA.
  User: Volodymyr
  Date: 10.07.2018
  Time: 0:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Post</title>
    <link rel="stylesheet" href="./assets/style.css" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <%--For fa-fa icons--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="${pageContext.request.contextPath}/blog">BLOG</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/signout"><span class="glyphicon glyphicon-log-out"></span> Sign Out</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <li><a href="${pageContext.request.contextPath}/posts"><i class="far fa-newspaper"></i> My Posts</a></li>
                <li><a href="${pageContext.request.contextPath}/drafts"><i class="fas fa-pen-square"></i> My Drafts</a></li>
                <li class="nav-item active"><a href="${pageContext.request.contextPath}/addpost"><i class="fas fa-plus"></i> Add</a></li>
            </ul>
        </div>
    </nav>
</header>
<div class="row add-post-form">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Add Post</h3>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/addpost" id="addPostForm" enctype="multipart/form-data">
                <div class="panel-body">
                    <input  required type="text" name="title" id="title" class="form-control" placeholder="Title"><br>
                    <textarea required name="description" form="addPostForm" placeholder="Enter post description here..." rows="3" class="form-control resize-none"></textarea><br>
                    <textarea required name="content" form="addPostForm" placeholder="Enter text content here..." rows="4" class="form-control resize-none"></textarea><br>
                    <div class="form-group">
                        <label class="control-label">Choose a picture</label>
                        <input required="required" class="form-control-file" type="file" name="uploadImage" id="uploadImage">
                    </div><br>
                    <label class="control-label">Choose tags (hold ctrl to select more than one)</label>
                    <select required multiple class="form-control" name="tags" id="tags">
                        <c:forEach items="${requestScope.tags}" var="tag">
                            <option class="form-control" value=${tag.id}>${tag.tag}</option>
                        </c:forEach>
                    </select><br>
                    <input onclick="form.action='${pageContext.request.contextPath}/addpost?type=post'" type="submit" value="Publish" class="btn btn-success">
                    <input onclick="form.action='${pageContext.request.contextPath}/addpost?type=draft'" type="submit" value="To Draft" class="btn btn-success">
                </div>
            </form>
            <div class="panel-footer">
                <div class="success-message">
                    ${requestScope.successAdd}
                </div>
                <div class="error-message">
                    ${requestScope.errors}
                </div>
            </div>
        </div>
    </div>
</div>
<!--jQuery plugin-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
