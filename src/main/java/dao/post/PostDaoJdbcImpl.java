package dao.post;

import dao.tag.TagDaoJdbcImpl;
import datastorage.DataStorageJDBC;
import model.Post;
import model.Role;
import model.Tag;
import model.User;
import service.user.UserServiceImpl;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PostDaoJdbcImpl implements IPostDao {

    public PostDaoJdbcImpl() {
    }

    @Override
    public Post insertPost(Post post) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "INSERT INTO post (title, description, content, image_url, date_of_creation, date_of_publication, user_id, tags_id) VALUES (?,?,?,?,?,?,?,?)";

        Date dateOfPublication;
        if (post.getDateOfPublication() != null)
            dateOfPublication = Date.valueOf(post.getDateOfPublication());
        else
            dateOfPublication = null;

        List<Integer> tagsId = post.getTags().stream().map(Tag::getId).collect(Collectors.toList());
        Array tagsIdArray = dataStorageJDBC.getConnection().createArrayOf("integer", tagsId.toArray());

        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, post.getTitle());
        statement.setString(2, post.getDescription());
        statement.setString(3, post.getContent());
        statement.setString(4, post.getImageURL());
        statement.setDate(5, Date.valueOf(post.getDateOfCreation()));
        statement.setDate(6, dateOfPublication);
        statement.setInt(7, post.getAuthor().getId());
        statement.setArray(8, tagsIdArray);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return post;
    }

    @Override
    public Post getPost(int id) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        Post post = new Post();
        String query = "SELECT * FROM post INNER JOIN \"user\" ON user_id = \"user\".id  WHERE post.id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        post.setId(resultSet.getInt("id"));
        post.setTitle(resultSet.getString("title"));
        post.setDescription(resultSet.getString("description"));
        post.setContent(resultSet.getString("content"));
        post.setImageURL(resultSet.getString("image_url"));
        post.setDateOfCreation(resultSet.getDate("date_of_creation").toLocalDate());

        LocalDate dateOfPublication;
        if (resultSet.getDate("date_of_publication") != null)
            dateOfPublication = resultSet.getDate("date_of_publication").toLocalDate();
        else
            dateOfPublication = null;
        post.setDateOfPublication(dateOfPublication);

        post.setAuthor(new User(
                resultSet.getInt("user_id"),
                resultSet.getString("user_name"),
                resultSet.getString("email"),
                resultSet.getString("password"),
                Role.valueOf(resultSet.getString("role")),
                resultSet.getBoolean("active")
        ));

        TagDaoJdbcImpl tagDaoJdbc = new TagDaoJdbcImpl();
        Integer[] tagsId = (Integer[]) resultSet.getArray("tags_id").getArray();
        List<Tag> tags = new ArrayList<>();
        for (int i = 0; i < tagsId.length; i++) {
            tags.add(tagDaoJdbc.getTag(tagsId[i]));
        }
        post.setTags(tags);

        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return post;
    }

    @Override
    public Post updatePost(Post post) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "UPDATE post SET title=?, description=?, content=?, image_url=?, date_of_publication=?, tags_id=? WHERE id=?";

        Date dateOfPublication;
        if (post.getDateOfPublication() != null)
            dateOfPublication = Date.valueOf(post.getDateOfPublication());
        else
            dateOfPublication = null;

        List<Integer> tagsId = post.getTags().stream().map(Tag::getId).collect(Collectors.toList());
        Array tagsIdArray = dataStorageJDBC.getConnection().createArrayOf("integer", tagsId.toArray());

        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, post.getTitle());
        statement.setString(2, post.getDescription());
        statement.setString(3, post.getContent());
        statement.setString(4, post.getImageURL());
        statement.setDate(5, dateOfPublication);
        statement.setArray(6, tagsIdArray);
        statement.setInt(7, post.getId());
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return post;
    }

    @Override
    public void deletePost(int id) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "DELETE FROM post WHERE id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setInt(1, id);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
    }

    @Override
    public List<Post> getAll() throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        List<Post> posts = new ArrayList<>();
        String query = "SELECT * FROM post INNER JOIN \"user\" ON user_id = \"user\".id";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            LocalDate dateOfPublication;
            if (resultSet.getDate("date_of_publication") != null)
                dateOfPublication = resultSet.getDate("date_of_publication").toLocalDate();
            else
                dateOfPublication = null;

            TagDaoJdbcImpl tagDaoJdbc = new TagDaoJdbcImpl();
            Integer[] tagsId = (Integer[]) resultSet.getArray("tags_id").getArray();
            List<Tag> tags = new ArrayList<>();
            for (int i = 0; i < tagsId.length; i++) {
                tags.add(tagDaoJdbc.getTag(tagsId[i]));
            }
            posts.add(new Post.Builder()
                    .setId(resultSet.getInt("id"))
                    .setTitle(resultSet.getString("title"))
                    .setDescription(resultSet.getString("description"))
                    .setContent(resultSet.getString("content"))
                    .setImageURL(resultSet.getString("image_url"))
                    .setDateOfCreation(resultSet.getDate("date_of_creation").toLocalDate())
                    .setDateOfPublication(dateOfPublication)
                    .setAuthor(new User(resultSet.getInt("user_id"),
                            resultSet.getString("user_name"),
                            resultSet.getString("email"),
                            resultSet.getString("password"),
                            Role.valueOf(resultSet.getString("role")),
                            resultSet.getBoolean("active")))
                    .setTags(tags).build()
            );
        }
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return posts;
    }

    @Override
    public List<Post> getPublishedPosts() throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        List<Post> posts = new ArrayList<>();
        String query = "SELECT * FROM post INNER JOIN \"user\" ON user_id = \"user\".id WHERE date_of_publication IS NOT NULL ORDER BY date_of_publication DESC";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            TagDaoJdbcImpl tagDaoJdbc = new TagDaoJdbcImpl();
            Integer[] tagsId = (Integer[]) resultSet.getArray("tags_id").getArray();
            List<Tag> tags = new ArrayList<>();
            for (int i = 0; i < tagsId.length; i++) {
                tags.add(tagDaoJdbc.getTag(tagsId[i]));
            }
            posts.add(new Post.Builder()
                    .setId(resultSet.getInt("id"))
                    .setTitle(resultSet.getString("title"))
                    .setDescription(resultSet.getString("description"))
                    .setContent(resultSet.getString("content"))
                    .setImageURL(resultSet.getString("image_url"))
                    .setDateOfCreation(resultSet.getDate("date_of_creation").toLocalDate())
                    .setDateOfPublication(resultSet.getDate("date_of_publication").toLocalDate())
                    .setAuthor(new User(resultSet.getInt("user_id"),
                            resultSet.getString("user_name"),
                            resultSet.getString("email"),
                            resultSet.getString("password"),
                            Role.valueOf(resultSet.getString("role")),
                            resultSet.getBoolean("active")))
                    .setTags(tags).build()
            );
        }
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return posts;
    }

    @Override
    public List<Post> getPublishedPostsByAuthor(int authorId) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        List<Post> posts = new ArrayList<>();
        String query = "SELECT * FROM post INNER JOIN \"user\" ON user_id = \"user\".id WHERE date_of_publication IS NOT NULL AND user_id = " + authorId + " ORDER BY date_of_publication DESC";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            TagDaoJdbcImpl tagDaoJdbc = new TagDaoJdbcImpl();
            Integer[] tagsId = (Integer[]) resultSet.getArray("tags_id").getArray();
            List<Tag> tags = new ArrayList<>();
            for (int i = 0; i < tagsId.length; i++) {
                tags.add(tagDaoJdbc.getTag(tagsId[i]));
            }
            posts.add(new Post.Builder()
                    .setId(resultSet.getInt("id"))
                    .setTitle(resultSet.getString("title"))
                    .setDescription(resultSet.getString("description"))
                    .setContent(resultSet.getString("content"))
                    .setImageURL(resultSet.getString("image_url"))
                    .setDateOfCreation(resultSet.getDate("date_of_creation").toLocalDate())
                    .setDateOfPublication(resultSet.getDate("date_of_publication").toLocalDate())
                    .setAuthor(new User(resultSet.getInt("user_id"),
                            resultSet.getString("user_name"),
                            resultSet.getString("email"),
                            resultSet.getString("password"),
                            Role.valueOf(resultSet.getString("role")),
                            resultSet.getBoolean("active")))
                    .setTags(tags).build()
            );
        }
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return posts;
    }

    public List<Post> getDraftsByAuthor(int authorId) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        UserServiceImpl userService = new UserServiceImpl();
        List<Post> posts = new ArrayList<>();
        String query = "SELECT * FROM post WHERE date_of_publication IS NULL AND user_id = " + authorId + " ORDER BY date_of_creation DESC";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            TagDaoJdbcImpl tagDaoJdbc = new TagDaoJdbcImpl();
            Integer[] tagsId = (Integer[]) resultSet.getArray("tags_id").getArray();
            List<Tag> tags = new ArrayList<>();
            for (int i = 0; i < tagsId.length; i++) {
                tags.add(tagDaoJdbc.getTag(tagsId[i]));
            }
            posts.add(new Post.Builder()
                    .setId(resultSet.getInt("id"))
                    .setTitle(resultSet.getString("title"))
                    .setDescription(resultSet.getString("description"))
                    .setContent(resultSet.getString("content"))
                    .setImageURL(resultSet.getString("image_url"))
                    .setDateOfCreation(resultSet.getDate("date_of_creation").toLocalDate())
                    .setDateOfPublication(null)
                    .setAuthor(userService.getUser(resultSet.getInt("user_id")))
                    .setTags(tags).build()
            );
        }
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return posts;
    }

    @Override
    public void publishDraft(int draftId) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "UPDATE post SET date_of_publication=? WHERE id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setDate(1, Date.valueOf(LocalDate.now()));
        statement.setInt(2, draftId);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
    }

}
