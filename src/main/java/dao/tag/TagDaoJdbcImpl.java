package dao.tag;

import datastorage.DataStorageJDBC;
import model.Tag;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TagDaoJdbcImpl implements ITagDao {

    public TagDaoJdbcImpl() {
    }

    @Override
    public Tag insertTag(Tag tag) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "INSERT INTO tag (tag) VALUES (?)";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, tag.getTag());
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return tag;
    }

    @Override
    public Tag getTag(int id) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        Tag tag = new Tag();
        String query = "SELECT * FROM tag WHERE id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        tag.setId(resultSet.getInt("id"));
        tag.setTag(resultSet.getString("tag"));
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return tag;
    }

    @Override
    public Tag updateTag(Tag tag) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "UPDATE tag SET tag = ? WHERE id = ?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, tag.getTag());
        statement.setInt(2, tag.getId());
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return tag;
    }

    @Override
    public void deleteTag(int id) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "DELETE FROM tag WHERE id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setInt(1, id);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
    }

    @Override
    public List<Tag> getAll() throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        List<Tag> tags = new ArrayList<>();
        String query = "SELECT * FROM tag";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            tags.add(new Tag(
                    resultSet.getInt("id"),
                    resultSet.getString("tag")
            ));
        }
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return tags;
    }
}
