package dao.tag;

import model.Tag;
import model.User;

import java.sql.SQLException;
import java.util.List;

public interface ITagDao {
    Tag insertTag(Tag tag) throws SQLException;
    Tag getTag(int id) throws SQLException;
    Tag updateTag(Tag tag) throws SQLException;
    void deleteTag(int id) throws SQLException;
    List<Tag> getAll() throws SQLException;
}
