package dao.user;

import model.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserDao {
    User insertUser(User user) throws SQLException;
    User getUser(int id) throws SQLException;
    User updateUser(User user) throws SQLException;
    void deleteUser(int id) throws SQLException;
    List<User> getAll() throws SQLException;
    User getUserByEmail(String email) throws SQLException;
    void banUser(int userId) throws SQLException;
    void unBanUser(int userId) throws SQLException;
}
