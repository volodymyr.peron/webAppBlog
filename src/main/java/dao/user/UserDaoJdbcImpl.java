package dao.user;

import datastorage.DataStorageJDBC;
import model.Role;
import model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoJdbcImpl implements IUserDao {

    public UserDaoJdbcImpl() {
        //default constructor
    }

    @Override
    public User insertUser(User user) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "INSERT INTO \"user\" (user_name, email, password, role, active) VALUES (?,?,?,?::role,?)";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1,user.getUserName());
        statement.setString(2, user.getEmail());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getRole().name());
        statement.setBoolean(5, user.isActive());
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return user;
    }

    @Override
    public User getUser(int id) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        User user = new User();
        String query = "SELECT * FROM \"user\" WHERE id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setInt(1, id);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        user.setId(resultSet.getInt("id"));
        user.setUserName( resultSet.getString("user_name"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setRole(Role.valueOf(resultSet.getString("role")));
        user.setActive(resultSet.getBoolean("active"));
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return user;
    }

    @Override
    public User updateUser(User user) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "UPDATE \"user\" SET user_name = ?, email = ?, password = ?, role = ?::role, active = ? WHERE id = ?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, user.getUserName());
        statement.setString(2, user.getEmail());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getRole().name());
        statement.setBoolean(5, user.isActive());
        statement.setInt(6, user.getId());
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return user;
    }

    @Override
    public void deleteUser(int id) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "DELETE FROM \"user\" WHERE id=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setInt(1, id);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
    }

    @Override
    public List<User> getAll() throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        List<User> users = new ArrayList<>();
        String query = "SELECT * FROM \"user\" WHERE role != 'ADMIN' ORDER BY id ASC";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()){
            users.add(new User(
                    resultSet.getInt("id"),
                    resultSet.getString("user_name"),
                    resultSet.getString("email"),
                    resultSet.getString("password"),
                    Role.valueOf(resultSet.getString("role")),
                    resultSet.getBoolean("active")
            ));
        }
        resultSet.close();
        statement.close();
        dataStorageJDBC.getConnection().close();
        return users;
    }

    public User getUserByEmail(String email) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        User user = new User();
        String query = "SELECT * FROM \"user\" WHERE email=?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setString(1, email);
        ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()) {
            user.setId(resultSet.getInt("id"));
            user.setUserName(resultSet.getString("user_name"));
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            user.setActive(resultSet.getBoolean("active"));
            resultSet.close();
            statement.close();
            dataStorageJDBC.getConnection().close();
            return user;
        }
        else
            return null;
    }

    @Override
    public void banUser(int userId) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "UPDATE \"user\" SET active = ? WHERE id = ?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setBoolean(1, false);
        statement.setInt(2, userId);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
    }

    @Override
    public void unBanUser(int userId) throws SQLException {
        DataStorageJDBC dataStorageJDBC = DataStorageJDBC.getInstance();
        String query = "UPDATE \"user\" SET active = ? WHERE id = ?";
        PreparedStatement statement = dataStorageJDBC.getConnection().prepareStatement(query);
        statement.setBoolean(1, true);
        statement.setInt(2, userId);
        statement.executeUpdate();
        statement.close();
        dataStorageJDBC.getConnection().close();
    }

}
