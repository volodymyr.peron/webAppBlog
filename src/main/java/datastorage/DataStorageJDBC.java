package datastorage;

import org.apache.log4j.Logger;
import servlet.AddPostServlet;

import java.sql.*;

public class DataStorageJDBC {
    final static Logger log = Logger.getLogger(DataStorageJDBC.class.getName());

    private static final String URL = DataStorageProperties.getUrl();
    private static final String LOGIN = DataStorageProperties.getLogin();
    private static final String PASSWORD = DataStorageProperties.getPassword();

    private Connection connection;

    private static volatile DataStorageJDBC instance;

    synchronized public static DataStorageJDBC getInstance() throws SQLException {
        if (instance == null) {
            instance = new DataStorageJDBC();
        } else if (instance.getConnection().isClosed()) {
            instance = new DataStorageJDBC();
        }
        return instance;
    }

    private DataStorageJDBC() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            log.error(e.getMessage());
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
