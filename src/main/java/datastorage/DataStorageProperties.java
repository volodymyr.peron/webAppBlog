package datastorage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DataStorageProperties {
    private static Properties properties = new Properties();
    private static final String DATABASE_PROPERTIES_FILE_PATH = "database.properties";

    private DataStorageProperties() {
        throw new IllegalStateException("Utility class");
    }

    public static String getUrl(){
        String url = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(DATABASE_PROPERTIES_FILE_PATH)) {
            properties.load(inputStream);
            url = properties.getProperty("databaseUrl");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }
    public static String getLogin(){
        String login = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(DATABASE_PROPERTIES_FILE_PATH)) {
            properties.load(inputStream);
            login = properties.getProperty("databaseLogin");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return login;
    }
    public static String getPassword(){
        String password = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(DATABASE_PROPERTIES_FILE_PATH)) {
            properties.load(inputStream);
            password = properties.getProperty("databasePassword");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return password;
    }
}
