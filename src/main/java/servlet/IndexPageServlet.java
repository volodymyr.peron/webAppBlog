package servlet;

import model.Post;
import org.apache.log4j.Logger;
import service.post.PostServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet({"/index", ""})
public class IndexPageServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(IndexPageServlet.class.getName());
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Post> posts =  new PostServiceImpl().getPublishedPosts();
            request.setAttribute("posts", posts);
            request.getRequestDispatcher("notauthorized/index.jsp").forward(request, response);
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath()+"/error?type=general");
        }

    }
}
