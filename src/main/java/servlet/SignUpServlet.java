package servlet;


import dao.user.UserDaoJdbcImpl;
import org.apache.log4j.Logger;
import service.security.BCryptEncoder;
import service.user.UserServiceImpl;
import service.validation.ModelValidator;
import model.Role;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/signup")
public class SignUpServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(SignUpServlet.class.getName());
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ModelValidator modelValidator = new ModelValidator();
        UserServiceImpl userService = new UserServiceImpl();
        User user = new User();
        List<String> errorMessages = new ArrayList<>();
        user.setRole(Role.USER);
        user.setActive(true);
        user.setUserName(req.getParameter("userName"));
        user.setEmail(req.getParameter("email"));
        user.setPassword(req.getParameter("password"));
        //Check email on uniqueness
        try {
            if(userService.getUserByEmail(req.getParameter("email")) != null)
                errorMessages.add("Email already exist");
        } catch (SQLException e) {
            log.error(e.getMessage());
            resp.sendRedirect(req.getContextPath()+"/error?type=general");
        }
        //Check password verification
        if(!req.getParameter("password").equals(req.getParameter("passwordConfirmation")))
            errorMessages.add("Password confirmation does not match");
        //validate user
        errorMessages.addAll(modelValidator.validate(user));
        //there aren't any errors - insert user and go to login page with message about success of registration
        if(errorMessages.isEmpty()) {
            try {
                user.setPassword(BCryptEncoder.getHashPassword(req.getParameter("password")));
                userService.insertUser(user);
            } catch (SQLException e) {
                log.error(e.getMessage());
                resp.sendRedirect(req.getContextPath()+"/error?type=general");
            }
            req.setAttribute("successSignUp", "Your account has been successfully created!");
            req.getRequestDispatcher("notauthorized/signin.jsp").forward(req, resp);
        }
        //there are some errors - go to sign up page with validation error messages
        else {
            req.setAttribute("errors", errorMessages);
            req.getRequestDispatcher("notauthorized/signup.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("notauthorized/signup.jsp").forward(req, resp);
    }
}
