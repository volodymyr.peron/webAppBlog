package servlet;

import model.Post;
import model.Tag;
import model.User;
import org.apache.log4j.Logger;
import service.post.PostServiceImpl;
import service.tag.TagServiceImpl;
import service.tag.TagServiceProxy;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@WebServlet("/editpost")
@MultipartConfig
public class EditPostServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(EditPostServlet.class.getName());
    private final String SAVE_IMAGE_DIRECTORY = "image";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PostServiceImpl postService = new PostServiceImpl();
        Post post = new Post();
        int postId = Integer.parseInt(request.getParameter("postId"));
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String content = request.getParameter("content");
        String[] tagsId = request.getParameterValues("tags");

        List<Tag> tags = null;
        try {
            tags = new TagServiceImpl().getTagsByListOfStringId(Arrays.asList(tagsId));
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath() + "/error?type=general");
        }

        Part filePart = request.getPart("uploadImage");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        String resultFileName;
        String uploadPathForImageInServer = request.getServletContext().getRealPath("") + SAVE_IMAGE_DIRECTORY;
        String uuidFile = UUID.randomUUID().toString();
        resultFileName = uuidFile + "." + fileName;
        String imageUrlInServer = uploadPathForImageInServer + File.separator + resultFileName;
        filePart.write(imageUrlInServer);

        Post updatedPost = new Post();
        if (!Objects.equals(request.getParameter("dateOfPublication"), ""))
            updatedPost.setDateOfPublication(LocalDate.now());
        updatedPost.setId(postId);
        updatedPost.setTags(tags);
        updatedPost.setImageURL(SAVE_IMAGE_DIRECTORY + File.separator + resultFileName);
        updatedPost.setContent(content);
        updatedPost.setTitle(title);
        updatedPost.setDescription(description);
        updatedPost.setAuthor((User) request.getSession().getAttribute("user"));
        try {
            postService.updatePost(updatedPost);
            response.sendRedirect(request.getContextPath() + "/posts");
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath() + "/error?type=general");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PostServiceImpl postService = new PostServiceImpl();
        TagServiceProxy tagService = new TagServiceProxy();
        Post post;
        List<Tag> tags;
        try {
            tags = tagService.getAll();
            post = postService.getPost(Integer.parseInt(request.getParameter("postId")));
            request.setAttribute("post", post);
            request.setAttribute("tags", tags);
            request.getRequestDispatcher("authorized/edit-post.jsp").forward(request, response);
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath() + "/error?type=general");
        }
    }
}
