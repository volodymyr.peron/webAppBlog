package servlet;

import org.apache.log4j.Logger;
import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(UsersServlet.class.getName());
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserServiceImpl userService = new UserServiceImpl();
        try {
            request.setAttribute("users", userService.getAll());
            request.getRequestDispatcher("authorized/users.jsp").forward(request, response);
        } catch (SQLException e) {
           log.error(e.getMessage());
           response.sendRedirect(request.getContextPath()+"/error?type=general");
        }
    }
}
