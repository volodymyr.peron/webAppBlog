package servlet;

import model.Tag;
import org.apache.log4j.Logger;
import service.tag.TagServiceProxy;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/edittag")
public class EditTagServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(EditPostServlet.class.getName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TagServiceProxy tagService = new TagServiceProxy();
        Tag tag = new Tag();
        tag.setId(Integer.parseInt(req.getParameter("tagId")));
        tag.setTag(req.getParameter("updateTag"));
        try {
            tagService.updateTag(tag);
            resp.sendRedirect(req.getContextPath() + "/tags");
        } catch (SQLException e) {
            log.error(e.getMessage());
            resp.sendRedirect(req.getContextPath() + "/error?type=general");
        }
    }

}
