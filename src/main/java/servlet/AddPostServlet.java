package servlet;

import model.Post;
import model.Tag;
import model.User;
import org.apache.log4j.Logger;
import service.post.PostServiceImpl;
import service.tag.TagServiceImpl;
import service.tag.TagServiceProxy;
import service.validation.ModelValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@WebServlet("/addpost")
@MultipartConfig
public class AddPostServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(AddPostServlet.class.getName());
    private final String SAVE_IMAGE_DIRECTORY = "image";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PostServiceImpl postService = new PostServiceImpl();
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String content = request.getParameter("content");
        String[] tagsId = request.getParameterValues("tags");

        List<Tag> tags = null;
        try {
            tags = new TagServiceImpl().getTagsByListOfStringId(Arrays.asList(tagsId));
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath() + "/error?type=general");
        }

        Part filePart = request.getPart("uploadImage");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        String resultFileName;
        String uploadPathForImageInServer = request.getServletContext().getRealPath("") + SAVE_IMAGE_DIRECTORY;

        File uploadDirectoryForImageInServer = new File(uploadPathForImageInServer);
        if (!uploadDirectoryForImageInServer.exists()) {
            uploadDirectoryForImageInServer.mkdir();
        }
        String uuidFile = UUID.randomUUID().toString();
        resultFileName = uuidFile + "." + fileName;
        String imageUrlInServer = uploadPathForImageInServer + File.separator + resultFileName;
        filePart.write(imageUrlInServer);

        Post post = new Post();
        post.setTitle(title);
        post.setDescription(description);
        post.setContent(content);
        post.setTags(tags);
        post.setDateOfCreation(LocalDate.now());
        if (request.getParameter("type").equals("post"))
            post.setDateOfPublication(LocalDate.now());
        else {
            post.setDateOfPublication(null);
        }
        post.setImageURL(SAVE_IMAGE_DIRECTORY + File.separator + resultFileName);
        HttpSession httpSession = request.getSession();
        post.setAuthor((User) httpSession.getAttribute("user"));
        ModelValidator modelValidator = new ModelValidator();
        List<String> errors = modelValidator.validate(post);
        if (errors.isEmpty()) {
            try {
                postService.insertPost(post);
                request.setAttribute("successAdd", "Your post has been successfully added!");
                doGet(request, response);
            } catch (SQLException e) {
                log.error(e.getMessage());
                response.sendRedirect(request.getContextPath() + "/error?type=general");
            }
        } else {
            request.setAttribute("errors", errors);
            doGet(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TagServiceProxy tagService = new TagServiceProxy();
        List<Tag> tags;
        try {
            tags = tagService.getAll();
            request.setAttribute("tags", tags);
            request.getRequestDispatcher("authorized/add-post.jsp").forward(request, response);

        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath() + "/error?type=general");
        }
    }
}
