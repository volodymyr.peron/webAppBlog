package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signout")
public class SignOutServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getSession() != null || request.getSession().getAttribute("user") != null) {
            request.getSession().invalidate();
        }
        request.setAttribute("successSignOut", "You have successfully logged out!");
        request.getRequestDispatcher("notauthorized/signin.jsp").forward(request, response);
    }
}
