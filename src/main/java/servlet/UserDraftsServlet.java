package servlet;

import model.Post;
import model.User;
import org.apache.log4j.Logger;
import service.post.PostServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/drafts")
public class UserDraftsServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(UserDraftsServlet.class.getName());
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PostServiceImpl postService = new PostServiceImpl();
        User user = (User) request.getSession().getAttribute("user");
        try {
            List<Post> usersDrafts = postService.getDraftsByAuthor(user.getId());
            request.setAttribute("usersDrafts", usersDrafts);
            request.getRequestDispatcher("authorized/user-drafts.jsp").forward(request, response);
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath()+"/error?type=general");
        }
    }
}
