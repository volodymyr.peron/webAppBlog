package servlet;

import model.Tag;
import org.apache.log4j.Logger;
import service.tag.TagServiceProxy;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/addtag")
public class AddTagServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(AddTagServlet.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TagServiceProxy tagService = new TagServiceProxy();
        Tag tag = new Tag();
        tag.setTag(request.getParameter("tag"));
        try {
            tagService.insertTag(tag);
            response.sendRedirect(request.getContextPath() + "/tags");
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath() + "/error?type=general");
        }
    }
}
