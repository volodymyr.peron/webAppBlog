package servlet;

import model.User;
import org.apache.log4j.Logger;
import service.security.BCryptEncoder;
import service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/signin")
public class SignInServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(SignInServlet.class.getName());
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserServiceImpl userService = new UserServiceImpl();
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if(email.isEmpty() || password.isEmpty()) {
            request.setAttribute("errors", "Please input all fields!");
            request.getRequestDispatcher("notauthorized/signin.jsp").forward(request, response);
        }
        else{
            try {
                User user = userService.getUserByEmail(email);
                    if (user != null && BCryptEncoder.checkPassword(password, user.getPassword())) {
                        if(user.isActive()) {
                            HttpSession session = request.getSession();
                            session.setAttribute("user", user);
                            response.sendRedirect(request.getContextPath()+"/posts");
                        }
                        else
                            response.sendRedirect(request.getContextPath()+"/error?type=ban");
                    }
                    else {
                        request.setAttribute("errors", "Incorrect password or email!");
                        request.getRequestDispatcher("notauthorized/signin.jsp").forward(request, response);
                    }
            } catch (SQLException e) {
                log.error(e.getMessage());
                response.sendRedirect(request.getContextPath()+"/error?type=general");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("notauthorized/signin.jsp").forward(request, response);
    }
}
