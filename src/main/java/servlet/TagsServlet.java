package servlet;

import org.apache.log4j.Logger;
import service.tag.TagServiceProxy;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/tags")
public class TagsServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(TagsServlet.class.getName());

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TagServiceProxy tagService = new TagServiceProxy();
        try {
            request.setAttribute("tags", tagService.getAll());
            request.getRequestDispatcher("authorized/tags.jsp").forward(request, response);
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath() + "/error?type=general");
        }
    }
}
