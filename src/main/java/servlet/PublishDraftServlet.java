package servlet;

import org.apache.log4j.Logger;
import service.post.PostServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/publishdraft")
public class PublishDraftServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(PublishDraftServlet.class.getName());
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PostServiceImpl postService = new PostServiceImpl();
        try {
            postService.publishDraft(Integer.parseInt(request.getParameter("draftId")));
            response.sendRedirect(request.getContextPath()+"/drafts");
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath()+"/error?type=general");
        }
    }
}
