package servlet;

import model.Post;
import org.apache.log4j.Logger;
import service.post.PostServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/post")
public class UserReadMoreServlet extends HttpServlet {
    final static Logger log = Logger.getLogger(UserReadMoreServlet.class.getName());
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PostServiceImpl postService = new PostServiceImpl();
        try {
            Post post = postService.getPost(Integer.parseInt(request.getParameter("postId")));
            request.setAttribute("post", post);
            request.getRequestDispatcher("authorized/user-read-more.jsp").forward(request, response);
        } catch (SQLException e) {
            log.error(e.getMessage());
            response.sendRedirect(request.getContextPath()+"/error?type=general");
        }
    }
}
