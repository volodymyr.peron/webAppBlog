package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/error")
public class ErrorServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String errorType = request.getParameter("type");
        if(errorType.equals("general"))
            request.getRequestDispatcher("notauthorized/error.jsp").forward(request, response);
        if(errorType.equals("ban"))
            request.getRequestDispatcher("notauthorized/ban.jsp").forward(request, response);
    }
}
