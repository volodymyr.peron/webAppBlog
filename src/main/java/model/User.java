package model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {
    private int id;
    @NotNull(message = "User name can not be null")
    @Size(min = 3, message = "User name must contain a minimum of 3 characters")
    private String userName;
    @NotNull(message = "Email can not be null")
    @Size(min = 3, message = "Email must contain a minimum of 3 characters")
    @Email(message = "Illegal email")
    private String email;
    @NotNull(message = "Password can not be null")
    @Size(min = 8, message = "Password must contain a minimum of 8 characters")
    private String password;
    private Role role;
    private boolean active;

    public User() {
    }

    public User(int id, String userName, String email, String password, Role role, boolean active) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.role = role;
        this.active = active;
    }

    private User(User target) {
        if (target != null) {
            this.id = target.id;
            this.userName = target.userName;
            this.email = target.email;
            this.password = target.password;
            this.role = target.role;
            this.active = target.active;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public User clone() {
        return new User(this);
    }
}
