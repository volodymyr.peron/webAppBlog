package model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

public class Post {
    private int id;
    @NotEmpty(message = "Title can not to be empty")
    private String title;
    @NotEmpty(message = "Description can not to be empty")
    private String description;
    @NotEmpty(message = "Content can not to be empty")
    private String content;
    @NotNull(message = "Choose image")
    private String imageURL;
    private LocalDate dateOfCreation;
    private LocalDate dateOfPublication;
    private User author;
    @NotNull(message = "Choose tags")
    private List<Tag> tags;

    public Post() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public LocalDate getDateOfPublication() {
        return dateOfPublication;
    }

    public void setDateOfPublication(LocalDate dateOfPublication) {
        this.dateOfPublication = dateOfPublication;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public static class Builder {
        private int id;
        private String title;
        private String description;
        private String content;
        private String imageURL;
        private LocalDate dateOfCreation;
        private LocalDate dateOfPublication;
        private User author;
        private List<Tag> tags;

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setContent(String content) {
            this.content = content;
            return this;
        }

        public Builder setImageURL(String imageURL) {
            this.imageURL = imageURL;
            return this;
        }

        public Builder setDateOfCreation(LocalDate dateOfCreation) {
            this.dateOfCreation = dateOfCreation;
            return this;
        }

        public Builder setDateOfPublication(LocalDate dateOfPublication) {
            this.dateOfPublication = dateOfPublication;
            return this;
        }

        public Builder setAuthor(User author) {
            this.author = author;
            return this;
        }

        public Builder setTags(List<Tag> tags) {
            this.tags = tags;
            return this;
        }

        public Post build() {
            Post post = new Post();
            post.id = id;
            post.title = title;
            post.description = description;
            post.content = content;
            post.imageURL = imageURL;
            post.dateOfCreation = dateOfCreation;
            post.dateOfPublication = dateOfPublication;
            post.author = author;
            post.tags = tags;
            return post;
        }
    }
}
