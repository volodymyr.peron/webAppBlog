package iterator;

import model.User;

import java.util.List;
import java.util.stream.Collectors;

public class BannedUsersIterator implements Iterator {
    private List<User> bannedUsers;
    private int currentPostIndex;

    public BannedUsersIterator(List<User> users) {
        currentPostIndex = 0;
        bannedUsers = users.stream().filter(e -> !e.isActive()).collect(Collectors.toList());
    }

    @Override
    public boolean hasNext() {
        return bannedUsers.size() - 1 >= currentPostIndex;
    }

    @Override
    public Object next() {
        return bannedUsers.get(currentPostIndex++);
    }
}
