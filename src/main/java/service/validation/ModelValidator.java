package service.validation;

import model.Post;
import model.User;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ModelValidator {
    private ValidatorFactory factory;
    private Validator validator;
    private List<String> errorMessages;
    public ModelValidator() {
        factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        errorMessages = new ArrayList<>();
    }

    public List<String> validate(User user)
    {
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        for (ConstraintViolation<User> violation : violations) {
            errorMessages.add(violation.getMessage());
        }
        return errorMessages;
    }

    public List<String> validate(Post post){
        Set<ConstraintViolation<Post>> violations = validator.validate(post);
        for (ConstraintViolation<Post> violation : violations) {
            errorMessages.add(violation.getMessage());
        }
        return errorMessages;
    }
}
