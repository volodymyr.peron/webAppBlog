package service.security;

import model.Role;
import model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/users", "/tags", "/addtag", "/edittag", "/ban"})
public class AdminFilter implements Filter {
    public void destroy() {}

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        HttpSession httpSession = httpServletRequest.getSession();
        if(httpSession == null || httpSession.getAttribute("user") == null)
        {
            HttpServletResponse httpServletResponse = (HttpServletResponse) resp;
            httpServletResponse.sendRedirect("signin");
        }
        else{
            if(((User) httpSession.getAttribute("user")).getRole() != Role.ADMIN)
            {
                HttpServletResponse httpServletResponse = (HttpServletResponse) resp;
                httpServletResponse.sendRedirect("blog");
            }
            else
                chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {}
}