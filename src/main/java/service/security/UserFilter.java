package service.security;

import model.Role;
import model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/posts", "/drafts", "/blog", "/post", "/addpost", "/delete", "/editpost", "/publishdraft"})
public class UserFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession httpSession = httpServletRequest.getSession();
        if(httpSession == null || httpSession.getAttribute("user") == null)
        {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.sendRedirect("signin");
        }
        else
        {
            if(((User) httpSession.getAttribute("user")).getRole() != Role.USER)
            {
                HttpServletResponse httpServletResponse = (HttpServletResponse) response;
                httpServletResponse.sendRedirect("users");
            }
            else
                chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
