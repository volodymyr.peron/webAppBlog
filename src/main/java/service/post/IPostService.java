package service.post;

import model.Post;

import java.sql.SQLException;
import java.util.List;

public interface IPostService {
    Post insertPost(Post post) throws SQLException;
    Post getPost(int id) throws SQLException;
    Post updatePost(Post post) throws SQLException;
    void deletePost(int id) throws SQLException;
    List<Post> getAll() throws SQLException;
    List<Post> getPublishedPosts() throws SQLException;
    List<Post> getPublishedPostsByAuthor(int authorId) throws SQLException;
    List<Post> getDraftsByAuthor(int authorId) throws SQLException;
    void publishDraft(int draftId) throws SQLException;
}
