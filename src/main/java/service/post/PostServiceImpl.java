package service.post;

import dao.post.PostDaoJdbcImpl;
import model.Post;

import java.sql.SQLException;
import java.util.List;

public class PostServiceImpl implements IPostService  {

    private PostDaoJdbcImpl postDaoJdbc = new PostDaoJdbcImpl();

    @Override
    public Post insertPost(Post post) throws SQLException {
        return postDaoJdbc.insertPost(post);
    }

    @Override
    public Post getPost(int id) throws SQLException {
        return postDaoJdbc.getPost(id);
    }

    @Override
    public Post updatePost(Post post) throws SQLException {
        return postDaoJdbc.updatePost(post);
    }

    @Override
    public void deletePost(int id) throws SQLException {
        postDaoJdbc.deletePost(id);
    }

    @Override
    public List<Post> getAll() throws SQLException {
        return postDaoJdbc.getAll();
    }

    @Override
    public List<Post> getPublishedPosts() throws SQLException {
        return postDaoJdbc.getPublishedPosts();
    }

    @Override
    public List<Post> getPublishedPostsByAuthor(int authorId) throws SQLException {
        return postDaoJdbc.getPublishedPostsByAuthor(authorId);
    }

    @Override
    public List<Post> getDraftsByAuthor(int authorId) throws SQLException {
        return postDaoJdbc.getDraftsByAuthor(authorId);
    }

    @Override
    public void publishDraft(int draftId) throws SQLException {
        postDaoJdbc.publishDraft(draftId);
    }
}
