package service.user;

import dao.user.UserDaoJdbcImpl;
import iterator.BannedUsersIterator;
import iterator.Iterator;
import model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements IUserService {

    private UserDaoJdbcImpl userDaoJdbc = new UserDaoJdbcImpl();

    @Override
    public User insertUser(User user) throws SQLException {
        return userDaoJdbc.insertUser(user);
    }

    @Override
    public User getUser(int id) throws SQLException {
        return userDaoJdbc.getUser(id);
    }

    @Override
    public User updateUser(User user) throws SQLException {
        return userDaoJdbc.updateUser(user);
    }

    @Override
    public void deleteUser(int id) throws SQLException {
        userDaoJdbc.deleteUser(id);
    }

    @Override
    public List<User> getAll() throws SQLException {
        return userDaoJdbc.getAll();
    }

    @Override
    public List<User> getBannedUsers() throws SQLException {
        Iterator bannedUsersIterator = new BannedUsersIterator(getAll());
        List<User> bannedUsers = new ArrayList<>();
        while (bannedUsersIterator.hasNext()) {
            bannedUsers.add((User) bannedUsersIterator.next());
        }
        return bannedUsers;
    }

    @Override
    public User getUserByEmail(String email) throws SQLException {
        return userDaoJdbc.getUserByEmail(email);
    }

    @Override
    public void banUser(int userId) throws SQLException {
        userDaoJdbc.banUser(userId);
    }

    @Override
    public void unBanUser(int userId) throws SQLException {
        userDaoJdbc.unBanUser(userId);
    }

}
