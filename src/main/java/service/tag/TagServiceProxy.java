package service.tag;

import model.Tag;

import java.sql.SQLException;
import java.util.List;

public class TagServiceProxy implements ITagService {

    TagServiceImpl tagService;

    @Override
    public Tag insertTag(Tag tag) throws SQLException {
        lazyInitialization();
        return tagService.insertTag(tag);
    }

    @Override
    public Tag getTag(int id) throws SQLException {
        lazyInitialization();
        return tagService.getTag(id);
    }

    @Override
    public Tag updateTag(Tag tag) throws SQLException {
        lazyInitialization();
        return tagService.updateTag(tag);
    }

    @Override
    public void deleteTag(int id) throws SQLException {
        lazyInitialization();
        tagService.deleteTag(id);
    }

    @Override
    public List<Tag> getAll() throws SQLException {
        lazyInitialization();
        return tagService.getAll();
    }

    @Override
    public List<Tag> getTagsByListOfStringId(List<String> listOfTagsId) throws SQLException {
        lazyInitialization();
        return tagService.getTagsByListOfStringId(listOfTagsId);
    }

    private void lazyInitialization() {
        if (tagService == null) {
            tagService = new TagServiceImpl();
        }
    }
}
