package service.tag;

import dao.tag.TagDaoJdbcImpl;
import model.Tag;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TagServiceImpl implements ITagService{

    private TagDaoJdbcImpl tagDaoJdbc = new TagDaoJdbcImpl();

    @Override
    public Tag insertTag(Tag tag) throws SQLException {
        return tagDaoJdbc.insertTag(tag);
    }

    @Override
    public Tag getTag(int id) throws SQLException {
        return tagDaoJdbc.getTag(id);
    }

    @Override
    public Tag updateTag(Tag tag) throws SQLException {
        return tagDaoJdbc.updateTag(tag);
    }

    @Override
    public void deleteTag(int id) throws SQLException {
        tagDaoJdbc.deleteTag(id);
    }

    @Override
    public List<Tag> getAll() throws SQLException {
        return tagDaoJdbc.getAll();
    }

    @Override
    public List<Tag> getTagsByListOfStringId(List<String> listOfTagsId) throws SQLException {
        List<Tag> tags = new ArrayList<>();
        TagServiceImpl tagService = new TagServiceImpl();
        for (String tagId : listOfTagsId){
            tags.add(tagService.getTag(Integer.parseInt(tagId)));
        }
        return tags;
    }
}
