package service.tag;

import model.Tag;

import java.sql.SQLException;
import java.util.List;

public interface ITagService {
    Tag insertTag(Tag tag) throws SQLException;
    Tag getTag(int id) throws SQLException;
    Tag updateTag(Tag tag) throws SQLException;
    void deleteTag(int id) throws SQLException;
    List<Tag> getAll() throws SQLException;
    public List<Tag> getTagsByListOfStringId(List<String> listOfTagsId) throws SQLException;
}
