--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

-- Started on 2018-07-17 13:46:33

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2824 (class 0 OID 16448)
-- Dependencies: 197
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."user" (id, user_name, email, password, role, active) VALUES (45, 'andry', 'perwolod@yandex.ru', '$2a$12$wThX/mnKjl1gZunPbwd0NO7rZJpcOdjQgeqaZnQT6ZVeiF46yuLdC', 'USER', true);
INSERT INTO public."user" (id, user_name, email, password, role, active) VALUES (43, 'vova', 'perwolod@gmail.com', '$2a$12$UjYYKGlJPj16brbRSvU09u37ekFyfpHDQk6T8M5FDPZj8H9nh9K.y', 'USER', true);
INSERT INTO public."user" (id, user_name, email, password, role, active) VALUES (44, 'admin', 'admin@gmail.com', '$2a$12$ypoP5StB47PATwRCWJOjIeltHPhb8mbkQlEyjUFaEZHFwqeEhX5DG', 'ADMIN', true);


--
-- TOC entry 2826 (class 0 OID 16459)
-- Dependencies: 199
-- Data for Name: post; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.post (id, title, description, content, image_url, date_of_creation, date_of_publication, user_id, tags_id) VALUES (74, 'A French farmer helps kids develop healthy digital habits', 'Edit note: To celebrate the hard work, creativity and entrepreneurial spirit of app makers around the world, over the coming months we celebrate our Android community by featuring founders, product managers, designers and developers from around the world. We showcase their passions and also hear about what they do when they step away from their computers. Meet our next app maker Gregory Veret, a French organic farmer, co-founder and CEO of Xooloo and check out more IMakeApps stories on ', 'When he was 29 while leading his tech startup Xoolo Gregory went back to school to learn agriculture and bought a 65-acre island to start a farm. His passion for helping children grow is consistent throughout his life both through his app that helps kids become good digital citizens, and his farm that provides food to Paris schools and daycares.', 'image\3e107548-5f0d-46bf-a999-b0de3adf8d44.blog-6.jpg', '2018-07-17', '2018-07-17', 43, '{13}');
INSERT INTO public.post (id, title, description, content, image_url, date_of_creation, date_of_publication, user_id, tags_id) VALUES (77, 'Putting machine learning into the hands of every advertiser', 'The ways people get things done are constantly changing, from finding the closest coffee shop to organizing family photos. Earlier this year, we explored how machine learning is being used to improve our consumer products and help people get stuff done.', 'Consumers today are more curious, more demanding, and they expect to get things done faster because of mobile. As a result, they expect your ads to be helpful and personalized. Doing this isnt easy, especially at scale. Thats why were introducing responsive search ads. Responsive search ads combine your creativity with the power of Googles machine learning to help you deliver relevant, valuable ads.

Simply provide up to 15 headlines and 4 description lines, and Google will do the rest. By testing different combinations, Google learns which ad creative performs best for any search query. So people searching for the same thing might see different ads based on context.

We know this kind of optimization works: on average, advertisers who use Googles machine learning to test multiple creative see up to 15 percent more clicks.', 'image\6391504a-481e-4a48-9449-49d2b6e7724a.blog-4.jpg', '2018-07-17', '2018-07-17', 45, '{20,21}');
INSERT INTO public.post (id, title, description, content, image_url, date_of_creation, date_of_publication, user_id, tags_id) VALUES (73, 'Celebrating 50 years of the Hayward ', 'In the early to mid-1900s, few would have imagined that the derelict space between Waterloo and Hungerford Bridge on Londons South Bank would become home to an iconic landmark, an integral part of modern British culture, and an enduring bastion of modern and contemporary art: The Hayward Gallery.', 'But in 1968, music, fashion and art were flourishing in London. Pink Floyd played to thousands in Hyde Park, Carnaby Street was the center of the fashion world, and the Hayward Gallery opened the doors to its first exhibition a major retrospective of the paintings of Henri Matisse.

Now to mark the 50th anniversary of its opening, the Hayward Gallery is providing exclusive access to its archives for the very first time with a Google Arts & Culture project called Hayward Gallery at 50: Uncovering the Archive. In an exclusive online tour, you can explore 1,000+ previously unseen architectural plans, installation sketches and photographs and glimpse behind the scenes of some of the most highly-regarded art exhibitions of the past half century. 
The collection brings together artifacts from 50 of Hayward previous exhibitions, from the opening show in 1968 through 2005 70-artist show Africa Remix: contemporary art of a continent and up until 2014''s Martin Creed blockbuster Whats the point of it?.', 'image\a0f82886-631f-4b48-9351-cd4a17c909d9.blog-2.jpg', '2018-07-17', '2018-07-17', 43, '{13}');


--
-- TOC entry 2828 (class 0 OID 16523)
-- Dependencies: 201
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.tag (id, tag) VALUES (14, 'Travel');
INSERT INTO public.tag (id, tag) VALUES (12, 'Health');
INSERT INTO public.tag (id, tag) VALUES (20, 'Culture');
INSERT INTO public.tag (id, tag) VALUES (21, 'Arts');
INSERT INTO public.tag (id, tag) VALUES (11, 'Sport');
INSERT INTO public.tag (id, tag) VALUES (13, 'Other');


--
-- TOC entry 2834 (class 0 OID 0)
-- Dependencies: 198
-- Name: post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.post_id_seq', 77, true);


--
-- TOC entry 2835 (class 0 OID 0)
-- Dependencies: 200
-- Name: tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tag_id_seq', 21, true);


--
-- TOC entry 2836 (class 0 OID 0)
-- Dependencies: 196
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 45, true);


-- Completed on 2018-07-17 13:46:34

--
-- PostgreSQL database dump complete
--

